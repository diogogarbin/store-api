package br.edu.unisep.store.domain.dto;

import lombok.Data;

@Data
public class RegisterProductDto {
    private final String name;

    private final String description;

    private final Float price;

    private final String brand;

    private final Integer status;
}
