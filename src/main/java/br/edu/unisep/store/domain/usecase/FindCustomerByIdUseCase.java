package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.domain.builder.CustomerBuilder;
import br.edu.unisep.store.domain.dto.CustomerDto;
import br.edu.unisep.store.domain.validator.FindCustomerByIdValidator;


public class FindCustomerByIdUseCase {

    public CustomerDto execute(Integer id) throws IllegalArgumentException {

        var validator = new FindCustomerByIdValidator();
        validator.validate(id);

        var dao = new CustomerDao();
        var customer = dao.findById(id);

        var builder = new CustomerBuilder();
        return builder.from(customer);
    }
}